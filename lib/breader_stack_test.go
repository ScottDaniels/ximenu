
package ximenu

import(
	"fmt"
	"os"
	"strings"
	"testing"
)

func TestBreader( t *testing.T ) {
	br := NewBReaderStack( 25 )
	err := br.Open( "brinput1.txt" )
	failOnErr(t, err, "could not open file" )

	for {
		r, err := br.Next()
		if err != nil {
			fmt.Fprintf( os.Stderr, "error from next(): %s\n", err )
			break
		}

		r = strings.Trim( r, "\n" )
		tokens := strings.Split( r, ":" )
		if tokens[0] == "import" && len(tokens) > 1  {
			fmt.Fprintf( os.Stderr, "opening: %s\n", tokens[1] )
			err := br.Open( tokens[1] )
			if err != nil {
				fmt.Fprintf( os.Stderr, "open failed: %s: %s\n", tokens[1], err )
			} else {
				fmt.Fprintf( os.Stderr, "open ok for: %s\n", tokens[1] )
			}
		} else {
			r = strings.Trim( r, "\n" )
			fmt.Fprintf( os.Stderr, "read: %s\n", r )
		}
	}
}

func TestBreaderFail( t *testing.T ) {
	br := NewBReaderStack( 25 )
	err := br.Open( "betternotbethere.txt" )
	failIfFalse(t, err != nil, "file not there didn't return an error" )
}
