
package ximenu

import (
	"testing"
)

func TestJReader( t *testing.T ) {

	d, err := JLoadXIM( "./test.xim" ) 
	failOnErr( t, err, "load failed" )

	d.Dump()
}
