/*
	Abstract:	A breader stack is a buffered reader which supports "pushing"
				multiple files such that import directives can be processes
				with little effort on the user's part. The breader supports
				a Next() method which reads the next newline separated record
				from the input. On error, the file is closed, and if there is
				a stacked open file it is popped and read from.

				CAUTION: the final record in a file MUST be new line terminated
						or it will be ignored. Some editors have a really
						nasty habit of dropping the newline on the last record.

	Date:		25 April 2023
	Author:		E. Scott Daniels
*/

// this really should be its own package
package ximenu

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
)


/*
	Implements a simple LIFO to manage nesed imbeds and provide a search
	path for open.
*/
type BReaderStack struct {
	sync.Mutex
	files	[]*os.File
	readers	[]*bufio.Reader
	path	[]string
	live	*bufio.Reader
	idx		int
	err		error
}

func NewBReaderStack( cap int ) *BReaderStack {
	return &BReaderStack {
		readers:	make( []*bufio.Reader, cap ),
		files:		make( []*os.File, cap ),
		path:		make( []string, 0 ),
		idx:		0,
		err:		nil,
	}
}


/*
	Opens the file and if successful pushes it onto the
	top of the reader stack. The first record from the
	new call will be returnd when Next() is invoked.

	If the path has been defined, and the filename does
	not begin with a '/' or "./", then the filename is
	appended to each path until it can be opened or the
	path options are exhausted.
*/
func (brs *BReaderStack) Open( fName string ) error {
	if brs == nil {
		return fmt.Errorf("no reader stack")
	}

	brs.Lock()
	defer brs.Unlock()

	var err error
	var f *os.File

	hasPath := fName[0:1]  == "/" || fName[0:2]  == "./"
	if !hasPath && len( brs.path ) > 0 {
		for _, p := range brs.path {
			f, err = os.Open( p + "/" + fName )
			if err == nil {
				break
			}
		}
	} else {
		f, err = os.Open( fName )
	}

	if err != nil {
		return err
	}

	brs.files[brs.idx] = f
	brs.live = bufio.NewReader( f )
	brs.readers[brs.idx] = brs.live
	brs.idx++

	return nil
}

/*
	Makes checking for end of file easier.
*/
func isEOF( err error ) bool {
	if err != nil {
		return err == io.EOF
	}

	return false
}

/*
	SetPath allows the user to pass in a path string of the
	form <path>:<path>:....
*/
func (brs *BReaderStack) SetPath( path string ) {
	if brs == nil || path == "" {
		return
	}

	brs.path = strings.Split( path, ":" )
}

/*
	Return the next record from the active reader. If there is
	an error, we check to see that it's an EOF and if so we
	close up the file, pop it off the stack, and read from the
	"paused" reader. If there is an error, the reader stack is
	tainted and error will always be returned for any subsequent
	call. The files are closed at the time of the error.
*/
func (brs *BReaderStack) Next() (string, error) {
	if brs == nil {
		return "", fmt.Errorf("nil pointer to reader stack instance")
	}

	brs.Lock()	// cannot defer the unlock, so be careful

	if brs.err != nil {		// already buggered, remind the caller
		brs.Unlock()
		return "", brs.err
	}

	rec, err := brs.live.ReadString( '\n' )
	if err == nil {
		brs.Unlock()
		return rec, nil	// all is well, send it out
	}

	if isEOF( err ) {	// on eof, pop the stack and if more, set up to read from them
		if brs.idx > 0 {				// idx should always be > 0, but be parinoid
			brs.idx--				// now at the currently open file
			brs.files[brs.idx].Close()
			brs.readers[brs.idx] = nil
			if brs.idx > 0 {			// back up live and read
				brs.live = brs.readers[brs.idx-1]
				brs.Unlock()			// must unlock before return; cannot defer
				return brs.Next()
			}
		}

		brs.err = err
		brs.Unlock()
		return "", err
	}	

	brs.err = err		// something bad happened, record and close up shop
	for ; brs.idx > 0; {
		brs.idx--
		brs.files[brs.idx].Close()
	}
	return "", err
}
