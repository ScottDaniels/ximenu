
/*
	Abstract:	Unit tests for the description reader.

	Date:		22 April 2023
	Author:		E. Scott Daniels
*/

package ximenu

import (
	"fmt"
	"os"
	"testing"
)

func TestDreaderSupport( t *testing.T ) {
	eCount := 0
	s := "   1	# a comment"
	dfb := defrockValue( s )
	fmt.Fprintf( os.Stderr, "defrock of: %q  yields: %q\n", s, dfb )
	eCount += failIfFalse(t, dfb == "1", "defrock failed (1)" )

	s = `"#ffffbb"   # a comment`	// should strip only the commeht and quotes
	dfb = defrockValue( s )
	fmt.Fprintf( os.Stderr, "defrock of: %q  yields: %q\n", s, dfb )
	eCount += failIfFalse(t, dfb == "#ffffbb", "defrock failed (1q)" )

	s = `"helloWorld"`	// should strip quotes
	dfb = defrockValue( s )
	fmt.Fprintf( os.Stderr, "defrock of: %q  yields: %q\n", s, dfb )
	eCount += failIfFalse(t, dfb == "helloWorld", "defrock failed (2q)" )

	s = `should not pick up all`
	dfb = defrockValue( s )
	fmt.Fprintf( os.Stderr, "defrock of: %q  yields: %q\n", s, dfb )
	eCount += failIfFalse(t, dfb == "should", "defrock failed (nq)" )

	s = "   1	"
	dfb = defrockValue( s )
	fmt.Fprintf( os.Stderr, "defrock of: %q  yields: %q\n", s, dfb )
	eCount += failIfFalse(t, dfb == "1", "defrock failed (2)" )

	s = "1	"
	dfb = defrockValue( s )
	fmt.Fprintf( os.Stderr, "defrock of: %q  yields: %q\n", s, dfb )
	eCount += failIfFalse(t, dfb == "1", "defrock failed (3)" )

	s = "1"
	dfb = defrockValue( s )
	fmt.Fprintf( os.Stderr, "defrock of: %q  yields: %q\n", s, dfb )
	eCount += failIfFalse(t, dfb == "1", "defrock failed (4)" )

	abortIfFalse(t, eCount == 0, "previous errors prevent further testing" )

	s = " 84306, 1140 	#comment"
	x, y := getIntPair( s )
	fmt.Fprintf( os.Stderr, "get int pair of %q yields: %d %d\n", s, x, y )
	eCount += failIfFalse(t, x == 84306, "get pair x was bad (1)" )
	eCount += failIfFalse(t, y == 1140, "get pair y was bad (1)" )

	s = " 84306,1140 	#comment"
	x, y = getIntPair( s )
	fmt.Fprintf( os.Stderr, "get int pair of %q yields: %d %d\n", s, x, y )
	eCount += failIfFalse(t, x == 84306, "get pair x was bad (2)" )
	eCount += failIfFalse(t, y == 1140, "get pair y was bad (2)" )

	s = "84306,1140   #comment"
	x, y = getIntPair( s )
	fmt.Fprintf( os.Stderr, "get int pair of %q yields: %d %d\n", s, x, y )
	eCount += failIfFalse(t, x == 84306, "get pair x was bad (3)" )
	eCount += failIfFalse(t, y == 1140, "get pair y was bad (3)" )

	s = "84306,1140"
	x, y = getIntPair( s )
	fmt.Fprintf( os.Stderr, "get int pair of %q yields: %d %d\n", s, x, y )
	eCount += failIfFalse(t, x == 84306, "get pair x was bad (4)" )
	eCount += failIfFalse(t, y == 1140, "get pair y was bad (4)" )

	s = "84306,abc"
	x, y = getIntPair( s )
	fmt.Fprintf( os.Stderr, "get int pair of %q yields: %d %d\n", s, x, y )
	eCount += failIfFalse(t, x == 84306, "get pair x was bad (5)" )
	eCount += failIfFalse(t, y == 0, "get pair y was bad (45" )
}

func TestDreaderParse( t *testing.T ) {
	fmt.Fprintf( os.Stderr, "starting parse test\n" )
	_, err := DLoadXIM( "xtest.ximd" )
	fmt.Fprintf( os.Stderr, "err: %s\n", err )
	failIfFalse(t, err != nil, "expected error but didn't get one" )

	d, err := DLoadXIM( "test.ximd" )
	failOnErr(t, err, "didn't expect dparse error" )
	d.Dump()
}
