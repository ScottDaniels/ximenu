
/*
	Abstract:	Description struct and its methods along with the json reading
				bits should someone actually want to write the menu in json.

	Date:		21 April 2023
	Author:		E. Scott Daniels
*/

package ximenu

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

/*
menu {
	title: string
	sets [] {
		buttonSet {
			name:	string
			pos:	{ 
				x: int  
				y: int
			}
			geom:	{ 
				width: int
				height: int
			}
			visible: bool
			bgColour: string (#rrggbb)
			fgColour: string (#rrggbb)

			buttons: [] {
				kind:			string ({radio|spring|sticky})
				label:			string
				press:			string ({none|menu|cmd|quit})
				release:		string ({none|menu|cmd|quit})
				target:			string
			}
		}
	}
}
*/

/*
	.xim files are read into these structures anchored by the Description.
*/

type DescPosition struct{
	X int	`json:"x"`
	Y int	`json:"y"`
}

type DescGeom struct {
	Width	int	`json:"width"`
	Height	int	`json:"height"`
}

type DescButton struct {
	FgColour	string	`json:"fgcolour"`
	BgColour	string	`json:"bgcolour"`
	Kind		string	`json:"kind"`
	Label		string	`json:"label"`
	PressAction	string	`json:"press"`
	RelAction	string	`json:"release"`
	Target		string	`json:"target"`
	Geom		DescGeom `json:"geom"`
}

type DescBSet struct {
	Name		string			`json:"name"`
	Pos			DescPosition	`json:"pos"`
	Geom		DescGeom		`json:"geom"`
	BSize		DescGeom		`json:"bsize"`
	Visible		bool			`json:"visible"`
	BgColour	string			`json:"bgColour"`
	FgColour	string			`json:"fgColour"`
	Buttons		[]*DescButton	`json:"buttons"`
}

type Description struct {
	Title		string		`json:"title"`
	Sets		[]*DescBSet	`json:"sets"`
	FgColour	string		`json:"fgcolour"`
	BgColour	string		`json:"bgcolour"`
	Geom		DescGeom	`json:"geom"`
}

/*
	Opens the file and unmarshals the json into a struct.
*/
func JLoadXIM( fName string ) (*Description, error) {
	f, err := os.Open( fName )
	if err != nil {
		return nil, err
	}

	jBytes, err := ioutil.ReadAll( f )
	if err != nil {
		return nil, err
	}
	f.Close()

	desc := &Description{}
	err = json.Unmarshal( jBytes, desc )
	if err != nil {
		return nil, err
	}

	return desc, err
}

/*
	Debugging dump to stderr
*/
func (md *Description) Dump() {
	fmt.Fprintf( os.Stderr, "desc: title=%q\n", md.Title )
	fmt.Fprintf( os.Stderr, "desc: %d sets\n", len( md.Sets ) )

	for i, bs := range md.Sets {
		fmt.Fprintf( os.Stderr, "\n" )
		fmt.Fprintf( os.Stderr, "set %d: name=%q\n", i, bs.Name )
		fmt.Fprintf( os.Stderr, "  pos/geom: %d,%d %d X %d\n", bs.Pos.X, bs.Pos.X, bs.Geom.Width, bs.Geom.Height )
		fmt.Fprintf( os.Stderr, "  visible: %v\n", bs.Visible )
		fmt.Fprintf( os.Stderr, "  colours: %s  %s\n", bs.BgColour, bs.FgColour )
		fmt.Fprintf( os.Stderr, "  buttons: (%d)\n", len(bs.Buttons) )

		for j, btn := range bs.Buttons {
			fmt.Fprintf( os.Stderr, "\n" )
			fmt.Fprintf( os.Stderr, "    [%d] kind:    %q\n", j, btn.Kind )
			fmt.Fprintf( os.Stderr, "    [%d] label:   %q\n", j, btn.Label )
			fmt.Fprintf( os.Stderr, "    [%d] pAction: %q\n", j, btn.PressAction )
			fmt.Fprintf( os.Stderr, "    [%d] rAction: %q\n", j, btn.RelAction )
			fmt.Fprintf( os.Stderr, "    [%d] target:  %q\n", j, btn.Target )
		}
	}
}

/*
	The descriptor structs have public fields because of the json support
	thus these methods are not stritly needed. However, should we drop
	json support, and make the fields private, these become necessary.
	From that perspective, these should be used rather than accessing the
	fields directly.
*/

/*
	Gets a button set from the description. We don't panic on an out of
	range request so that the user may blindly request the next and
	stop when the result is nil. Once the description is created it is
	immutable and so the "iteration" by the user should be safe.
*/
func (md *Description) BSetN( n int ) *DescBSet {
	if md == nil {
		return nil
	}

	if n < 0 || n >= len( md.Sets ) {
		return nil
	}

	return md.Sets[n]
}

func (md *Description) Geometry( ) (width int, height int) {
	if md == nil {
		return 0, 0
	}

	return md.Geom.Width, md.Geom.Height
}

func (md *Description) MenuTitle( ) string {
	if md == nil {
		return ""
	}

	return md.Title
}


// --------------------------- set methods ------------------------------------------------

/*
	Gets a button from the set. This will also copy any uninitialised
	fields from the set.
*/
func (dbs *DescBSet) ButtonN( n int ) *DescButton {
	if dbs == nil {
		return nil
	}

	if n < 0 || n >= len( dbs.Buttons ) {
		return nil
	}


	dbtn := dbs.Buttons[n]	// fill in any missing info that we can snarf from the set
	if dbtn.Geom.Width <= 0 {
		dbtn.Geom.Width = dbs.BSize.Width
	}
	if dbtn.Geom.Height <= 0 {
		dbtn.Geom.Height = dbs.BSize.Height
	}

	if dbtn.FgColour == "" {
		dbtn.FgColour = dbs.FgColour
	}
	if dbtn.BgColour == "" {
		dbtn.BgColour = dbs.BgColour
	}

	return dbtn
}


/*
	Returns the button set colours -- used as defult if the button doesn't
	have a colour.
*/
func (dbs *DescBSet) Colours( n int ) (fgColour string, bgColour string ) {
	if dbs == nil {
		return "", ""
	}

	return dbs.FgColour, dbs.BgColour
}


func (dbs *DescBSet) Geometry() (x int, y int, width int, height int ) {
	if dbs == nil {
		return 0, 0, 0, 0
	}

	return dbs.Pos.X, dbs.Pos.Y, dbs.Geom.Width, dbs.Geom.Height
}

func (dbs *DescBSet) IsVisible() bool {
	if dbs == nil {
		return false
	}

	return dbs.Visible
}

/*
	Return the name of the set.
*/
func (dbs *DescBSet) BsName( ) string {
	if dbs == nil {
		return ""
	}

	return dbs.Name
}


// -------------------- button methods ---------------------------------------------

func (dbtn *DescButton) Colours() (fg string, bg string) {
	if dbtn == nil {
		return "", ""
	}

	return dbtn.FgColour, dbtn.BgColour
}


/*
	Dig out the kind and label. Kind is converted from the string
	given by the human (e.g. spring) to the internal menu type.
*/
func (dbtn *DescButton) KindLabel() ( kind XIButtonKind, label string) {
	if dbtn == nil {
		return UNKNOWN_BUTTON, ""
	}

	kind = UNKNOWN_BUTTON
	label = dbtn.Label
	switch dbtn.Kind {
		case "radio":	
			kind = RADIO_BUTTON

		case "spring":	
			kind = SPRING_BUTTON

		case "sticky":	
			kind = STICKY_BUTTON
	}

	return kind, label
}

/*
	Send back the dimensions of the button.
*/
func (dbtn *DescButton) Geometry() (width int, height int) {
	if dbtn == nil {
		return 0, 0
	}

	return dbtn.Geom.Width, dbtn.Geom.Height
}

func str2action( str string ) Action {
	switch str {
		case "none": return Nothing
		case "show": return Show
		case "hide": return Hide
		case "cmd":  return Command

		default:	return Nothing
	}
}

/*
	Return the press/release actions and the target. Actions are translated
	from the string passed in by the human to the  internal type.
*/
func (dbtn *DescButton) ActionsTarget() (pAction Action, rAction Action, rTarget string ) {
	if dbtn == nil {
		return Nothing, Nothing, ""
	}

	pAction = str2action( dbtn.PressAction )
	rAction = str2action( dbtn.RelAction )
	return pAction, rAction,  dbtn.Target
}
