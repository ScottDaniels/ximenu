/*
	Abstract:	Something to manage a group of buttons.

	Date:		18 April 2023
	Author:		E. Scott Daniels
*/

package ximenu

import( 
	sk "bitbucket.org/EScottDaniels/sketch"
)

/*
	Describes a set of buttons.
*/
type ButtonSet struct {
	name		string			// for debug and the like
	id			int				// sets have an ID which prefixes the button ID
	isVis		bool			// true if this set is currently painted
	nextX		int				// x,y for next button placement
	nextY		int
	dispWidth	int				// size of the set on the screen
	dispHeight	int
	dispX		int				// location in the root window
	dispY		int
	lastHeight	int				// height of last added button
	lastButton	*Button					// last button (radio) pressed
	bid2btn		map[int]*Button			// button action information by id
	bid2kind	map[int]XIButtonKind	// maps the button id to the kind of button
	nextBid		int						// next button id to be assigned
	uChan		chan *XImEvent
}

func NewButtonSet( name string, id int, x int, y int, width int, height int ) *ButtonSet {
	return &ButtonSet {
		name:			name,
		id:				id,
		isVis:			false,
		nextBid:		0,
		nextX:			0,
		nextY:			0,
		dispWidth:		width,
		dispHeight:		height,
		dispX:			x,
		dispY:			y,
		bid2btn:		make( map[int]*Button ),
		bid2kind:		make( map[int]XIButtonKind ), 
		uChan:			make( chan *XImEvent, 1024 ),
	}
}

/*
	Given a button ID (full or just 0xff portion) return the pointer
	to the button instance, or nil.
*/
func (bs *ButtonSet) Button( bid int ) *Button {
	if bs == nil {
		return nil
	}

	button, there := bs.bid2btn[bid]
	if there {
		return button
	}

	return nil
}

/*
	Return the display info for the set.
*/
func (bs *ButtonSet) DispInfo() (x int, y int, width int, height int) {
	return bs.dispX, bs.dispY, bs.dispWidth, bs.dispHeight
}

/*
	Create the listen channel that is given to the user to listen to.
*/
func (bs *ButtonSet) MkChan() chan *XImEvent {
	if bs == nil {
		return nil
	}

	bs.uChan = make( chan *XImEvent, 1024 )
	return bs.uChan
}

/*
	Given the grapics context (an interface) paint the buttons and other
	bits that are related to this set. Buttons are painted by inserting
	them into the subpage that was created for the set.
*/
func (bs *ButtonSet) Paint( gc sk.Graphics_api ) {
	if bs == nil || gc == nil {
		return
	}

	// future paint background
	// future paint title

	for _, btn := range bs.bid2btn {
		btn.Insert( gc )	
	}
}


/*
	Capture the last button changed in the set (for radio popping).
*/
func (bs *ButtonSet) SetLast( b *Button ) {
	if bs == nil {
		return
	}

	bs.lastButton = b
}

/*
	Toggle the state of the last changed button. Allows a set which is
	radio buttons to logically pop out the last depressed button. Returns
	the last button.
*/
func (bs *ButtonSet) ToggleLast( ) *Button {
	if bs == nil {
		return nil
	}

	b := bs.lastButton
	b.ToggleState()
	return b
}

